package com.example.jenkins.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class JenkinsRestController {

    @GetMapping(value = "/success")
    public String testSuccess() {
        return "Test success jenkins";
    }

    @GetMapping(value = "/fail")
    public String testFailure() {
        return "Test failure jenkins";
    }
}
