package com.example.jenkins;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class JenkinsApplicationTests {

	@Test
	public void testAddSuccess() {
		Assert.assertEquals(1+1, 2);
	}

	@Test
	public void testAddFailure() {
		Assert.assertEquals(1+1, 0);
	}

}
